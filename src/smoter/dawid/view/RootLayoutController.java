package smoter.dawid.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import smoter.dawid.MainApp;
import smoter.dawid.model.GNote;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class RootLayoutController {	
	private Stage dialogStage;
	private MainApp mainApp;
	private Map<String, ObservableList<GNote>> listy = new HashMap(); 
	
	@FXML
	MenuItem CloseItem;
	
	@FXML
	private void initialize() {
		CloseItem.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				Platform.exit();
				
			}
		});
	}
	
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	
	public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
	
	@FXML
	public boolean databaseImport() throws IOException{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Database");
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Database files", "*.sql"));
		
		File selectedFile = fileChooser.showOpenDialog(dialogStage);
		File oldFile = new File(System.getProperty("user.dir") + "/notes.db");
		
		if(selectedFile.exists()) {
			copyFile(new FileInputStream(selectedFile), new FileOutputStream(oldFile));
			return true;
		}
		return false;
	}
	
	@FXML
	public void databaseExport() throws FileNotFoundException, IOException {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Database files", ".sql"));
		System.out.println("OKI1");
		File file = fileChooser.showSaveDialog(dialogStage);
		System.out.println("OKI2");
		File dbFile = new File(System.getProperty("user.dir") + "/notes.db");
		
		System.out.println(file.getAbsolutePath());
		if(file != null && dbFile.exists()) {
			copyFile(new FileInputStream(dbFile), new FileOutputStream(file));
		}
	}
	
	private void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
	
}

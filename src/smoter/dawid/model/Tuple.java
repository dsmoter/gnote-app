package smoter.dawid.model;

import javafx.scene.control.Tab;
import javafx.scene.control.TableView;

/*
 * Tab tab: 			zakładka
 * TableView tabview:	widok tabelki 
 */

public class Tuple {
	private Tab tab;
	private TableView<GNote> tabview;
	
	public Tuple(Tab tab1, TableView<GNote> tabview1) {
		super();
		this.tab = tab1;
		this.tabview = tabview1;
	}

	public Tab getTab() {
		return tab;
	}

	public void setTab(Tab tab) {
		this.tab = tab;
	}

	public TableView<GNote> getTabview() {
		return tabview;
	}

	public void setTabview(TableView<GNote> tabview) {
		this.tabview = tabview;
	}
	
	
}

package smoter.dawid;

import java.io.IOException;
















import java.util.Arrays;
import java.util.List;

import smoter.dawid.model.GNote;
import smoter.dawid.model.J_Database;
import smoter.dawid.model.Zakladka;
import smoter.dawid.view.MainOverviewController;
import smoter.dawid.view.MessageAddDialogController;
import smoter.dawid.view.RootLayoutController;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

// 0 - nic do zmiany
// 1 - nowa
// 2 - zmodyfikowana

public class MainApp extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	private J_Database datab;
	
	
	public J_Database getDatabase() {
		return datab;
	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("GNote app");
		initRootLayout();
		showMainOverview();
		
	}
	
	public void initRootLayout() {
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			
			rootLayout = (BorderPane) loader.load();
			
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
			datab = new J_Database();
			scene.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					datab.closeConnection();
					
				}
			});
			RootLayoutController controller = loader.getController();
			controller.setDialogStage(primaryStage);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void showMainOverview() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/MainOverview.fxml"));
			
			AnchorPane mainOverview = (AnchorPane) loader.load();
//			final TabPane tabs = new TabPane();
//			final Button addButton = new Button("+");
//			
//			SplitPane tabelka = (SplitPane) mainOverview.getChildren().get(0);
//			AnchorPane tabelka2 = (AnchorPane) tabelka.getItems().get(1);
//			//System.out.println(tabelka2.getChildren().size());
//			TabPane tabelka3 =  (TabPane) tabelka2.getChildren().get(0);
//			
//			tabelka2.setTopAnchor(addButton, 10.0);
//			tabelka2.setLeftAnchor(addButton, 10.0);
//			
			//addButton.setOnAction(new EventHandler<ActionEvent>() {
				
			//});
			
			//tabelka2.getChildren().addAll(addButton);
			
			rootLayout.setCenter(mainOverview);
			
			MainOverviewController controller = loader.getController();
			controller.setMainApp(this);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public boolean showMessageAddDialog(GNote note, ObservableList<Zakladka> tabs) {	

		try {
			FXMLLoader 	loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/MessageAddDialog.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Note");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			MessageAddDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setMainApp(this);
			controller.setNote(note, tabs);
			
			dialogStage.showAndWait();
			
			return controller.isOkClicked();
		} catch (IOException e) {
			System.err.println("Błąd w AddEditNote");
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}

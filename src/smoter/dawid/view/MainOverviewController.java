package smoter.dawid.view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.Provider.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.util.Callback;
import javafx.util.Pair;

import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Flags.Flag;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.controlsfx.dialog.CommandLinksDialog;
import org.controlsfx.dialog.CommandLinksDialog.CommandLinksButtonType;

import smoter.dawid.MainApp;
import smoter.dawid.model.GNote;
import smoter.dawid.model.Tuple;
import smoter.dawid.model.Zakladka;

import com.sun.javafx.tk.Toolkit.Task;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;

public class MainOverviewController {
	private Properties props;
	private IMAPFolder inbox;
	private Session session;
	private IMAPStore store2;
	private MainApp mainApp;

	public static String USER = "";
	public static String PASSWORD = "";
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		ObservableList<GNote> wiadomosc = mainApp.getDatabase().getMessagesToTab("Main", 1);
		Tuple tup = this.creation("Main", wiadomosc);
		Tab mainTab = tup.getTab();
		mainTab.setClosable(false);
		tabs.add(new Zakladka(mainTab, 1,tup.getTabview()));
	}
	
	@FXML
	private TextField txField;
	@FXML
	private Button btn1;	
	@FXML
	private AnchorPane MainAnch;
	@FXML
	private TabPane tabpanel;
	@FXML
	private TableView<GNote> notesTable;
	@FXML
	private TableColumn<GNote, String> subjectColumn;
	@FXML
	private TableColumn<GNote, String> xCreatedColumn;
	@FXML
	private TableColumn<GNote, String> contentColumn;
	@FXML
	private TableColumn<GNote, String> XModifiedColumn;
	@FXML
	private TableColumn<GNote, String> XUniversallyUniqueIdentifierColumn;

	private ObservableList<GNote> notesData = FXCollections
			.observableArrayList();

	private ObservableList<Zakladka> tabs = FXCollections.observableArrayList();

	private Map<String, ObservableList<GNote>> listy = new HashMap();

	public ObservableList<GNote> getNotesData() {
		return notesData;
	}

	@FXML
	private void initialize() {
		tabpanel.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
		tabpanel.getTabs().stream().forEach(action -> {
			action.setOnClosed(foo);
		});
		
		
		
		props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		props.put("mail.imaps.ssl.checkserveridentity", "false");
		props.put("mail.imaps.ssl.trust", "*");
		
	}

	
	@FXML
	private boolean handleConnect() {
		session = Session.getDefaultInstance(props, null);
		try {
			store2 = (IMAPStore) session.getStore("imaps");
			
			if(USER.equals("") || PASSWORD.equals("")) {
				this.addUser();
			}
			
			if(!USER.equals("") || !PASSWORD.equals("")) {
				store2.connect("imap.gmail.com", USER,PASSWORD);
			} else {
				return false;
			}
			
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("Próba ustanowienia połączenia");
			alert.setContentText("Połączenie ustanowione");
			File b = new File(System.getProperty("user.dir"),"config.ini");
			if(!b.exists()) {
				try {
					b.createNewFile();
					FileWriter fstream = new FileWriter(b);
			        BufferedWriter out = new BufferedWriter(fstream);
			        out.write(USER);
			        out.newLine();
			        out.write(PASSWORD);
			        out.close();
				} catch (IOException e) {
					System.err.println("Bład przy tworzeniu config.ini");
					e.printStackTrace();
				}
			}
			
			alert.showAndWait();
			return true;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			
		} catch (MessagingException e) {
			e.printStackTrace();
			USER = "";
			PASSWORD = "";
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("Próba ustanowienia połączenia");
			alert.setContentText("Błędny login i/lub hasło.");
			alert.showAndWait();
		}
		return true;
	}

	
	
	@FXML
	private void synchronize() {
		Message[] messages = null;
		try {
			inbox = (IMAPFolder) store2.getFolder("Notes");
			inbox.open(Folder.READ_WRITE);
			FetchProfile fp = new FetchProfile();
			fp.add(IMAPFolder.FetchProfileItem.HEADERS);
			fp.add(IMAPFolder.FetchProfileItem.FLAGS);
			fp.add(IMAPFolder.FetchProfileItem.SIZE);
			fp.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);
			fp.add(IMAPFolder.FetchProfileItem.ENVELOPE);
			messages = inbox.getMessages();
			inbox.fetch(messages, fp);
			
			HashMap<String, GNote> localNotes = mainApp.getDatabase().getMessages();
			String head;
			String moDate;
		
			List<Message> serverNotes = new ArrayList<Message>(Arrays.asList(messages));

			// pobranie wiadomości z serwera
			for (Message message : serverNotes) {
				MimeMessage m = (MimeMessage) message;
				head = m.getHeader("X-Universally-Unique-Identifier", ":");
				moDate = m.getHeader("X-Modified-Date", ":");
				
				if(localNotes.containsKey(head)) {
					if(localNotes.get(head).getLongLastModifiedDate().equals(Long.parseLong(moDate))) {
						if(!localNotes.get(head).getLongXModifiedDate().equals(localNotes.get(head).getLongLastModifiedDate())) {
							// wyślij na serwer
							m.setFlag(Flags.Flag.DELETED, true);
							inbox.expunge();
							Message mes = createMimeMessage(head);
							mes.setFlag(Flag.SEEN, true);
							inbox.appendMessages(new Message[]{mes});
							mainApp.getDatabase().updateLastSyncDate(head);
						} 
					} else {
						if(localNotes.get(head).getLongXModifiedDate().equals(localNotes.get(head).getLongLastModifiedDate())) {
							// pobierz wiadomosc z serwera
							mainApp.getDatabase().updateMessage(m);
						} else {
							List<CommandLinksButtonType> links = Arrays.asList(new CommandLinksButtonType("Odrzuć lokalną wersję", "Operacja ta polega na pobraniu z serwera wiadomości. Efektem jest utracenie wersji znajdującej się obecnie w bazie danych.", true), new CommandLinksButtonType("Odrzuć wersję na serwerze", "Operacja ta polega na zaktualizowaniu wiadomości na serwerze do wiadomości z bazy danych. Efektem jest utracenie wersji znajdującej się obecnie na serwerze.", false), new CommandLinksButtonType("Pobierz obie wersje", "Operacja ta polega na pobraniu obu wiadomości." ,false),new CommandLinksButtonType("Anuluj", "Zostawiamy wiadomości w takim stanie jakie są teraz. Do synchronizacji wrócimy następnym razem.", false));
							CommandLinksDialog dlg = new CommandLinksDialog(links);
							dlg.initModality(Modality.WINDOW_MODAL);
							dlg.setTitle("Konflikt edycji");
							dlg.setHeaderText("Proszę wybrać sposób synchronizacji wiadomości");
							dlg.showAndWait();
							
							switch(dlg.getResult().getText()) {
								case "Odrzuć lokalną wersję":
									mainApp.getDatabase().updateMessage(m);
									break;
								case "Odrzuć wersję na serwerze":
									m.setFlag(Flags.Flag.DELETED, true);
									inbox.expunge();
									Message mes = createMimeMessage(head);
									mes.setFlag(Flag.SEEN, true);
									inbox.appendMessages(new Message[]{mes});
									mainApp.getDatabase().updateLastSyncDate(head);
									break;
								case "Pobierz obie wersje":
									String newID = mainApp.getDatabase().changeID(head);
									mainApp.getDatabase().updateMessage(m);
									localNotes.put(newID, new GNote());
									break;
									
							}
						}
					}
					localNotes.remove(head);
				} else {
					// pobierz wiadomości z serwera
					GNote note = new GNote(m);
					mainApp.getDatabase().insertMessage(note, 0);
					ObservableList<GNote> lista = listy.get(mainApp.getDatabase().getFolderName(note.getStringMessageGroup()));
					if(lista != null) {
						lista.add(note);
					}
					this.getAttachs(m);
				}
		}
			
			for(String key : localNotes.keySet()) {
				Message mes = createMimeMessage(key);
				mes.setFlag(Flag.SEEN, true);
				inbox.appendMessages(new Message[]{mes});
			}
			
			this.refreshList();
		} catch (MessagingException e) {
			System.err.println("Synchronization error.");
			e.printStackTrace();
		}
		
	}
	
	private void getAttachs(MimeMessage m) {
		Object content;
		try {
			content = m.getContent();
			 if (content.getClass().isAssignableFrom(MimeMultipart.class)) {
				 MimeMultipart multipart = (MimeMultipart) content;
				 for(int i=0;i<multipart.getCount();i++) {
					 BodyPart bodyPart = multipart.getBodyPart(i);
					 if(!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
			                if (bodyPart.getContent().getClass().equals(MimeMultipart.class)) {
			                    MimeMultipart mimemultipart = (MimeMultipart)bodyPart.getContent();
			                    for (int k=0;k<mimemultipart.getCount();k++) {
			                        if (mimemultipart.getBodyPart(k).getFileName() != null) {
			                            savefile(mimemultipart.getBodyPart(k).getFileName(),m.getHeader("X-Universally-Unique-Identifier", ":"), mimemultipart.getBodyPart(k).getInputStream());
			                            mainApp.getDatabase().updateAttachments(bodyPart, m.getHeader("X-Universally-Unique-Identifier", ":"));
			                        }
			                    }
			                }
			              continue;
				 }
					 savefile(bodyPart.getFileName(), m.getHeader("X-Universally-Unique-Identifier", ":"), bodyPart.getInputStream());
					 mainApp.getDatabase().updateAttachments(bodyPart, m.getHeader("X-Universally-Unique-Identifier", ":"));
			    }
			 }
		} catch (IOException e) {
			System.err.println("MainOverviewController: getAttachs IOException error.");
			e.printStackTrace();
			//return null;
		} catch (MessagingException e) {
			System.err.println("MainOverviewController: getAttachs MessagingException error.");
			e.printStackTrace();
			//return null;
		}
	   
	}
	
	private static void savefile(String FileName, String id, InputStream is) throws IOException {
		File b = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37));
		b.mkdirs();
        File f = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37) + "/" + FileName);
        FileOutputStream fos = new FileOutputStream(f);
        byte[] buf = new byte[4096];
        int bytesRead;
        while((bytesRead = is.read(buf))!=-1) {
            fos.write(buf, 0, bytesRead);
        }
        fos.close();
    }

	
	private Message createMimeMessage(String id) {
		
		List<String> dane = mainApp.getDatabase().getNoteData(id);
		ObservableList<String> files = mainApp.getDatabase().getNoteAttachs(id);
		if(dane.size() == 3) {
					
			MimeMessage message = new MimeMessage(session);
			String from = "smoter.dawid@gmail.com";
			try {
				message.setSubject(dane.get(0), "utf-8");
				message.setHeader("Message-ID", id);
				message.setHeader("MIME-Version", "1.0");
				message.setHeader("X-Created-Date", dane.get(1));
				message.setHeader("X-Modified-Date", dane.get(2));
				message.setHeader("X-Reminder-Time", "0");
				message.setHeader("X-Text-Type", "1");
				message.setHeader("X-Universally-Unique-Identifier", id);
				message.setFrom(new InternetAddress(from));		
				message.setText(dane.get(0), "utf-8");
				
				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(dane.get(0), "text/plain; charset=utf-8");
				
				if(files.size() != 0) {
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				for(String nazwa : files) {
					MimeBodyPart attachPart = new MimeBodyPart();
					
					try {
						attachPart.attachFile(System.getProperty("user.dir") + nazwa);
						multipart.addBodyPart(attachPart);
					} catch (IOException e) {
						System.err.println("MainOverviewController add attach error.");
						e.printStackTrace();
					}
				}
				
				message.setContent(multipart);
				}
				message.saveChanges();
				return message;
			} catch (MessagingException e) {
				System.err.println("MainOverviewController: createMimeMessage error.");
				e.printStackTrace();
				return null;
			}
			
		}
		return null;
		
	}
	
	@FXML
	private void refreshList() {
		for (Entry<String, ObservableList<GNote>> entry : listy.entrySet()) {
		    String key = entry.getKey();
		    int nr = mainApp.getDatabase().openTab(key);
		    ObservableList<GNote> lista = mainApp.getDatabase().getMessagesToTab(key, nr);
		    entry.getValue().setAll(lista);
		}
	}
	
	@FXML
	private void handleGetMsgs() throws MessagingException {
		Message[] messages = inbox.getMessages();
		FetchProfile fp = new FetchProfile();
		fp.add(IMAPFolder.FetchProfileItem.HEADERS);
		fp.add(IMAPFolder.FetchProfileItem.FLAGS);
		fp.add(IMAPFolder.FetchProfileItem.SIZE);
		fp.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);
		fp.add(IMAPFolder.FetchProfileItem.ENVELOPE);
		inbox.fetch(messages, fp);

		for (Message message : messages) {
			MimeMessage m = (MimeMessage) message;
			notesData.add(new GNote(m));
		}

	}

	/*
	 * Dodawanie nowej wiadomości
	 */
	@FXML
	private void handleAdd() {
		GNote newNote = new GNote();
		boolean okClicked = mainApp.showMessageAddDialog(newNote, tabs);
		if (okClicked) {
			
			// Znajdź zakładkę o takim ID jak w wiadomości
			Optional<Zakladka> zak = tabs
					.stream()
					.filter(abc -> {
						return abc.getID() == Integer.valueOf(newNote
								.getStringMessageGroup());
					}).findFirst();
			
			listy.get(zak.get().getName()).add(newNote);
			
			if (mainApp.getDatabase().insertMessage(newNote,1)) {
				try {
					mainApp.getDatabase().checkAttachments(newNote.getAttachsList(), newNote.getStringXUniversallyUniqueIdentifier());
				} catch (IOException e) {
					System.err.println("MainOverviewController: handleAdd -> checkAttachments error.");
					e.printStackTrace();
				}
			} 
		}
		
	}
	
	/*
	 * Edytowanie wiadomości
	 */
	@FXML
	private void handleEdit() {
		TableView<GNote> view = (TableView<GNote>) tabpanel.getSelectionModel().getSelectedItem().getContent();
		GNote newNote = view.getSelectionModel().getSelectedItem();
		if (newNote != null) {
			boolean okClicked = mainApp.showMessageAddDialog(newNote, tabs);
			if (okClicked) {
				String idZBazy = mainApp.getDatabase().getFolderID(newNote.getStringXUniversallyUniqueIdentifier());
				if(!idZBazy.equals(newNote.getStringMessageGroup())) {
					ObservableList<GNote> dousuniecia = listy.get(tabpanel.getSelectionModel().getSelectedItem().getText());
					for(GNote notka : dousuniecia) {
						if(notka.getStringXUniversallyUniqueIdentifier().equals(newNote.getStringXUniversallyUniqueIdentifier())) {
							dousuniecia.remove(notka);
							listy.get(mainApp.getDatabase().getFolderName(newNote.getStringMessageGroup())).add(newNote);
							break;
						}
					}
					
				} 
				
				mainApp.getDatabase().updateAfterEdit(newNote, false);
				try {
					mainApp.getDatabase().checkAttachments(newNote.getAttachsList(), newNote.getStringXUniversallyUniqueIdentifier());
				} catch (IOException e) {
					System.err.println("MainOverviewController: handleEdit -> checkAttachments error.");
					e.printStackTrace();
				}
			}
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("Wybór wiadomości");
			alert.setContentText("Nie wybrano wiadomości.");
			alert.showAndWait();
		}

	}

	@FXML
	private void addTab() {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Tab creation");
		dialog.setHeaderText("Tab creation");
		dialog.setContentText("Please enter tab name:");
		
		Optional<String> response = dialog.showAndWait();
		
		if (response.isPresent() && !response.get().equals("")) {
			int isOK = mainApp.getDatabase().createTab(response.get());
			if (isOK != 0) {
				ObservableList<GNote> lista = FXCollections.observableArrayList();
				Tuple tup = this.creation(response.get(),lista);
				Zakladka z = new Zakladka(tup.getTab(), isOK, tup.getTabview());
				tabs.add(z);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Information Dialog");
				alert.setHeaderText("Tworzenie zakładki");
				alert.setContentText("Wystąpił błąd. Nie wykonano tworzenia zakładki.");
				alert.showAndWait();
				System.err.println("Nie wykonano tworzenia taba");
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("Nazwa");
			alert.setContentText("Podaj dobrą nazwę zakładki");
			alert.showAndWait();
		}
	}

	private Tuple creation(String nazwa, ObservableList<GNote> lista) {
		final Tab tab = new Tab(nazwa);
		tab.setOnCloseRequest(foo);
		tabpanel.getTabs().add(tab);
		tabpanel.getSelectionModel().select(tab);
		TableView<GNote> tabview = new TableView<GNote>();
		tabview.setItems(lista);
		tab.setOnSelectionChanged(new EventHandler<Event>() {
			public void handle(Event t) {
				if(tab.isSelected()) {
					FilteredList<GNote> filteredData = new FilteredList<>(lista,p -> true);
					
					txField.textProperty().addListener((observable, oldValue, newValue) -> {
						filteredData.setPredicate(gnote -> {
							if(newValue == null || newValue.isEmpty()) {
								return true;
							}
							
							String lowerCaseFilter = newValue.toLowerCase();
							
							if(gnote.getSubject().toString().toLowerCase().indexOf(lowerCaseFilter) != -1) {
								return true;
							}
							
							return false;
						});
					});
					
					SortedList<GNote> sortedData = new SortedList<>(filteredData);
					sortedData.comparatorProperty().bind(tabview.comparatorProperty());
					
					tabview.setItems(sortedData);
				}
			}
		});
		
		FilteredList<GNote> filteredData = new FilteredList<>(lista,p -> true);
		
		txField.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(gnote -> {
				if(newValue == null || newValue.isEmpty()) {
					return true;
				}
				
				String lowerCaseFilter = newValue.toLowerCase();
				
				if(gnote.getSubject().toString().toLowerCase().indexOf(lowerCaseFilter) != -1) {
					return true;
				}
				
				return false;
			});
		});
		
		SortedList<GNote> sortedData = new SortedList<>(filteredData);
		sortedData.comparatorProperty().bind(tabview.comparatorProperty());
		
		tabview.setItems(sortedData);
		
		listy.put(nazwa, lista);

		
		List<String> columns = Arrays.asList("MessageID", "Subject",
				"Created Date", "Modified Date");
		List<String> variab = Arrays.asList(
				"XUniversallyUniqueIdentifierColumn", "subjectColumn",
				"xCreatedColumn", "XModifiedColumn");
		int columnIndex = 0;
		TableColumn[] tableColumns = new TableColumn[columns.size()];
		for (String columnName : columns) {
			tableColumns[columnIndex] = new TableColumn(columnName);
			String vab = variab.get(columnIndex);
			tableColumns[columnIndex].setId(vab);
			columnIndex++;
		}

		tableColumns[0].setCellValueFactory(setMesID);
		tableColumns[1].setCellValueFactory(setSubj);
		tableColumns[2].setCellValueFactory(setXCD);
		tableColumns[3].setCellValueFactory(setXMD);

		tabview.getColumns().setAll(tableColumns);
		tab.setContent(tabview);
		return new Tuple(tab, tabview);
	}

	@FXML
	private void openTab2() {
		List<String> lista = mainApp.getDatabase().getTabs();
		
		if(lista == null || lista.size() == 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			   alert.setTitle("Information Dialog");
			   alert.setHeaderText("Otwarcie zakładki nie powiodło się.");
			   alert.setContentText("W bazie danych nie ma storzonych zakładek.");
			   alert.showAndWait();
		} else {
		
			ChoiceDialog<String> dialog = new ChoiceDialog<String>("", lista);
			dialog.setTitle("Choice Input Dialog");
			dialog.setHeaderText("Zaimportuj zakładkę");
			dialog.setContentText("Wybierz zakładkę:");
			Optional<String> response2 = dialog.showAndWait();
			
			if (response2.isPresent()) {
			   if(listy.containsKey(response2.get())) {
				   Alert alert = new Alert(AlertType.INFORMATION);
				   alert.setTitle("Information Dialog");
				   alert.setHeaderText("Otwarcie zakładki nie powiodło się.");
				   alert.setContentText("Dana zakładka jest już otwarta");
				   alert.showAndWait();
			   } else {
				   openTab(response2.get());
			   }
				
			}
		}
	}
	
	
	private void openTab(String wybor) {
		if (wybor != "") {
			int isOK = mainApp.getDatabase().openTab(wybor);
			if (isOK != 0) {
				ObservableList<GNote> wiadomosc = mainApp.getDatabase().getMessagesToTab(wybor, isOK);
				Tuple tup = this.creation(wybor,wiadomosc);
				Tab tab = tup.getTab();
				tabs.add(new Zakladka(tab, isOK, tup.getTabview()));
				
				listy.put(wybor, wiadomosc);
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Information Dialog");
				alert.setHeaderText("Próba otwarcia zakładki");
				alert.setContentText("Wystąpił błąd. Nie wykonano twierania zakładki.");
				alert.showAndWait();
				System.err.println("Nie wykonano otwierania taba");
			}
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Information Dialog");
			alert.setHeaderText("Próba otwarcia zakładki.");
			alert.setContentText("Wystąpił błąd podczas otwierania zakładki: podaj dobrą nazwę.");
			alert.showAndWait();
			System.err.println("openTab: Podaj dobrą nazwę taba.");
		}

	}
	
	/* Callbacks */

	Callback<TableColumn.CellDataFeatures<GNote, String>, ObservableValue<String>> setSubj = (param) -> {
		if (param.getValue() != null) {
			return param.getValue().getSubject();
		} else {
			return new SimpleStringProperty("<no name>");
		}
	};

	Callback<TableColumn.CellDataFeatures<GNote, String>, ObservableValue<String>> setMesID = (param) -> {
		if (param.getValue() != null) {
			return param.getValue().getXUniversallyUniqueIdentifier();
		} else {
			return new SimpleStringProperty("<no name>");
		}
	};

	Callback<TableColumn.CellDataFeatures<GNote, String>, ObservableValue<String>> setXCD = (param) -> {
		if (param.getValue() != null) {
			return param.getValue().getGoodCreatedDate();
		} else {
			return new SimpleStringProperty("<no name>");
		}
	};

	Callback<TableColumn.CellDataFeatures<GNote, String>, ObservableValue<String>> setXMD = (param) -> {
		if (param.getValue() != null) {
			// return param.getValue().getGoodModifiedDate();
			return param.getValue().getxmd();
		} else {
			return new SimpleStringProperty("<no name>");
		}
	};
	
	private EventHandler<Event> foo = value -> {
		String nazwa = tabpanel.getSelectionModel().getSelectedItem().getText();
		tabs.stream()
			.filter(zak -> zak.getName().equals(nazwa))
			.map(zak -> tabs.remove(zak));
		listy.remove(nazwa);
	};
	
	
	
	public boolean addUser() {
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Login Dialog");
		dialog.setHeaderText("Login");
		
		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CLOSE);

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField username = new TextField();
		username.setPromptText("Username");
		PasswordField password = new PasswordField();
		password.setPromptText("Password");

		grid.add(new Label("Username:"), 0, 0);
		grid.add(username, 1, 0);
		grid.add(new Label("Password:"), 0, 1);
		grid.add(password, 1, 1);
		
		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);
		
		File b = new File(System.getProperty("user.dir"),"config.ini");
		if(b.exists()) {
			FileReader fstream;
			try {
				fstream = new FileReader(b);
				BufferedReader in = new BufferedReader(fstream);
		        
		        username.setText(in.readLine());
		        username.setDisable(true);
		        password.setText(in.readLine());
		        username.setDisable(true);
		        loginButton.setDisable(false);
		        in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
		}
		
		

		username.textProperty().addListener((observable, oldValue, newValue) -> {
		    loginButton.setDisable(newValue.trim().isEmpty());
		});
		
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == loginButtonType) {
		        return new Pair<>(username.getText(), password.getText());
		    }
		    return null;
		});
		
		dialog.getDialogPane().setContent(grid);
		Optional<Pair<String, String>> result = dialog.showAndWait();
		
		if(result.isPresent()){
		USER = result.get().getKey();
		PASSWORD = result.get().getValue();
		return true;
		} else {
			USER = "";
			PASSWORD = "";
			return false;
		}
	}
	
}

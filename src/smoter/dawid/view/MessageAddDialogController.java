package smoter.dawid.view;


import java.io.File;
import java.util.List;
import java.util.UUID;

import smoter.dawid.MainApp;
import smoter.dawid.model.GNote;
import smoter.dawid.model.Zakladka;
import smoter.dawid.model.Zalacznik;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;



public class MessageAddDialogController {
	@FXML
	private TextArea msgContent;
	@FXML
	private ListView<Zalacznik> attachs;
	
	@FXML
	private ChoiceBox<Zakladka> choiceTab;
	
	private ObservableList<Zalacznik> zalacz = FXCollections.observableArrayList();
	
	private GNote notatka;
	private Stage dialogStage;
	private boolean okClicked = false;
	private MainApp mainApp;

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	
	
	
	public boolean isOkClicked() {
		return okClicked;
	}
	
	@FXML
	private void initialize() {
		
		attachs.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
	}
	
	public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
	
	/* Pobranie notatki do edycji lub stworzenia */
	public void setNote(GNote note, ObservableList<Zakladka> tabs) {
		this.notatka = note;
		
		this.refreshAttachs(notatka.getStringXUniversallyUniqueIdentifier());
		msgContent.setText(notatka.getStringSubject());
		choiceTab.setItems(tabs);
		choiceTab.getSelectionModel().selectFirst();
	}
	
	/*
	 * Dodawanie nowej notatki
	 */
	
	@FXML
	private void handleOk() {
		if(isValid()) {
			
			// ustaw tresc
			notatka.setSubject(msgContent.getText());
			notatka.setMessageGroup(choiceTab.getSelectionModel().getSelectedItem().getID() + "");
			
			// ustaw messageID i X-Uniersally-Unique-Identifier
			if(notatka.getStringXUniversallyUniqueIdentifier()=="") {
				notatka.setXUniversallyUniqueIdentifier("<" + String.valueOf(UUID.randomUUID()) + "@email.android.com>");
			}
			
			// ustaw daty
			
			if(notatka.getStringXCreatedDate()=="") {
				Long epoch = new Long(System.currentTimeMillis());
				notatka.setXCreatedDate(epoch.toString());
			}
			
			String tmp = notatka.getStringXModifiedDate();
			Long epoch = new Long(System.currentTimeMillis());
			notatka.setXModifiedDate(epoch.toString());
			if(tmp == "") {
				notatka.setLastModifiedDate(epoch.toString());
			}
			
			// ustaw załączniki
			notatka.setAttachsList(zalacz);
			
			okClicked = true;
			dialogStage.close();
		}
	}
	
	@FXML
	private void handleCancelele() {
		dialogStage.close();
	}
	
	@FXML
	private void FilePicker() {
		FileChooser fileChooser = new FileChooser();
		 fileChooser.setTitle("Open Resource File");
		
		List<File> files = fileChooser.showOpenMultipleDialog(this.dialogStage);
		 if(files != null) {
			 for(File plik : files)
				zalacz.add(new Zalacznik(plik.getName(), plik.getPath()));
		 }
	}
	
	@FXML
	private void deleteAttachs() {
		 List<Integer> indeksy = attachs.getSelectionModel().getSelectedIndices();
		
		for(int ind : indeksy) {
			zalacz.remove(ind);
		}
	}
	
	private boolean isValid() {
		if(msgContent.getText() == null || msgContent.getText().length() == 0) {
			return false;
		}
		
		return true;
	}
	
	private void refreshAttachs(String id) {
		zalacz = this.mainApp.getDatabase().selectAttachs(id);
		attachs.setItems(zalacz);
		
	}
}

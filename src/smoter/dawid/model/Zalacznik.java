package smoter.dawid.model;

import java.io.InputStream;

public class Zalacznik {
	private String fileName;
	private String path;
	private InputStream in;
	
	public Zalacznik(String fileName, String path) {
		this.fileName = fileName;
		this.path = path;
	}
	
	public Zalacznik(String fileName, InputStream in) {
		this.fileName = fileName;
		this.in = in;
	}
	
	public Zalacznik(String fileName) {
		this.fileName = fileName;
		this.path = "";
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	public void setIn(InputStream in) {
		this.in = in;
	}
	
	public InputStream getIn() {
		return this.in;
	}
	
	@Override
	public String toString() {
		return this.fileName;
	}

	@Override
	public boolean equals(Object object) {
		boolean sameSame = false;
		
		if(object != null && object instanceof Zalacznik) {
			sameSame = this.fileName.equals(((Zalacznik) object).getFileName());
		}
		return sameSame;
	}
	
	

}

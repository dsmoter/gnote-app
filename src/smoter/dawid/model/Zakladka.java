package smoter.dawid.model;

import javafx.scene.control.Tab;
import javafx.scene.control.TableView;

public class Zakladka {

	private int ID;
	private String name;
	private Tab mainTab;
	private TableView<GNote> tabview;
	
	public Zakladka(Tab item, int index, TableView<GNote> tabview) {
		this.ID = index;
		this.name = item.getText();
		this.mainTab = item;
		this.tabview = tabview;
	}
	
	public TableView<GNote> getTabview() {
		return tabview;
	}

	public void setTabview(TableView<GNote> tabview) {
		this.tabview = tabview;
	}

	public Tab getMainTab() {
		return mainTab;
	}

	public void setMainTab(Tab mainTab) {
		this.mainTab = mainTab;
	}

	public String toString() {
		return name;
		
	}
	
	public String getName() {
		return name;
	}
	
	public int getID() {
		return ID;
	}
}

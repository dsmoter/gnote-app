package smoter.dawid.model;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class GNote {

	private StringProperty subject;
	private StringProperty contentType;
	private StringProperty MessageID;
	private StringProperty messageGroup;
	
	private ObservableList<Zalacznik> attachsList;
	private StringProperty xmd;
	private StringProperty XCreatedDate;
	private StringProperty XModifiedDate;
	private StringProperty LastModifiedDate;
	private StringProperty XUniformTypeIdentifier;
	private StringProperty XUniversallyUniqueIdentifier;
	
	
	
	public GNote(MimeMessage mes) {
		try {
			this.subject = new SimpleStringProperty(mes.getSubject());
			this.XCreatedDate = new SimpleStringProperty(mes.getHeader("X-Created-Date", ":"));
			this.XModifiedDate = new SimpleStringProperty(mes.getHeader("X-Modified-Date", ":"));
			this.LastModifiedDate = new SimpleStringProperty(mes.getHeader("X-Modified-Date", ":"));
			this.contentType = new SimpleStringProperty(mes.getContentType());
			Date date = new Date(Long.parseLong(XModifiedDate.get()));
			Format foramt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			this.xmd = new SimpleStringProperty(foramt.format(date));
			this.XUniformTypeIdentifier = new SimpleStringProperty(mes.getHeader("X-Uniform-Type-Identifier", ":"));
			this.XUniversallyUniqueIdentifier = new SimpleStringProperty(mes.getHeader("X-Universally-Unique-Identifier", ":"));
			this.MessageID = new SimpleStringProperty(mes.getMessageID());
			this.messageGroup = new SimpleStringProperty(mes.getHeader("MessageGroup", ":"));
			if(messageGroup.get() == null || messageGroup.get() == "null") {
				this.messageGroup.set("1");
			}
		} catch (MessagingException e) {
			System.err.println("Błąd podczas tworzenia GNote");
			e.printStackTrace();
		}
	}
	
	public GNote(String mesID, String noteName, String createdDate, String modifyDate, int folderID) {
		this.subject = new SimpleStringProperty(noteName);
		this.XCreatedDate = new SimpleStringProperty(createdDate);
		this.XModifiedDate = new SimpleStringProperty(modifyDate);
		Date date = new Date(Long.parseLong(modifyDate));
		Format foramt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.xmd = new SimpleStringProperty(foramt.format(date));
		this.contentType = new SimpleStringProperty("");
		this.XUniformTypeIdentifier = new SimpleStringProperty("");
		this.XUniversallyUniqueIdentifier = new SimpleStringProperty(mesID);
		this.MessageID = new SimpleStringProperty(mesID);
		this.messageGroup = new SimpleStringProperty(folderID+"");
	}
	
	public GNote(String mesID, String modifyDate, String lastDate) {
		this.XModifiedDate = new SimpleStringProperty(modifyDate);
		this.XUniversallyUniqueIdentifier = new SimpleStringProperty(mesID);
		this.LastModifiedDate = new SimpleStringProperty(lastDate);
	}
	
	public GNote() {
		this.subject = new SimpleStringProperty("");
		this.XCreatedDate = new SimpleStringProperty("");
		this.XModifiedDate = new SimpleStringProperty("");
		this.LastModifiedDate = new SimpleStringProperty("");
		this.xmd = new SimpleStringProperty("");
		this.contentType = new SimpleStringProperty("");
		this.XUniformTypeIdentifier = new SimpleStringProperty("");
		this.XUniversallyUniqueIdentifier = new SimpleStringProperty("");
		this.MessageID = new SimpleStringProperty("");
		this.messageGroup = new SimpleStringProperty("1");
	}

/* SUBJECT */
	public StringProperty getSubject() {
		return subject;
	}
	
	public String getStringSubject() {
		return subject.get();
	}


	public void setSubject(String subject) {
		this.subject.set(subject);
	}

/* LastModifiedDate */
	
	public StringProperty getLastModifiedDate() {
		return LastModifiedDate;
	}
	
	public String getStringLastModifiedDate() {
		return LastModifiedDate.get();
	}
	
	public void setLastModifiedDate(String lmd) {
		this.LastModifiedDate.set(lmd);
	}
	
	public Long getLongLastModifiedDate() {
		return Long.parseLong(LastModifiedDate.get());
	}
	
/* X-CREATED-DATE */
	public StringProperty getXCreatedDate() {
		return XCreatedDate;
	}
	
	public String getStringXCreatedDate() {
		return XCreatedDate.get();
	}
	
	public StringProperty getGoodCreatedDate() {
		 Date date = new Date(Long.parseLong(XCreatedDate.get()));
		 Format format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		 return new SimpleStringProperty(format.format(date));
		
		//Date data = new Date(Long.parseLong(XCreatedDate.get()));
		//return new SimpleStringProperty(data.toString());
	}
	
	public String getStringGoodCreatedDate() {
		Date date = new Date(Long.parseLong(XCreatedDate.get()));
		Format foramt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return foramt.format(date);
		//Date data = new Date(Long.parseLong(XCreatedDate.get()));
		//return data.toString();
	}

	public void setXCreatedDate(String xCreatedDate) {
		this.XCreatedDate.set(xCreatedDate);
	}
	
/* X-MODIFIED-DATE */
	public StringProperty getXModifiedDate() {
		return XModifiedDate;
	}

	public StringProperty getGoodModifiedDate() {
		Date date = new Date(Long.parseLong(XModifiedDate.get()));
		Format format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return new SimpleStringProperty(format.format(date));
		
		//Date data = new Date(Long.parseLong(XCreatedDate.get()));
		//return new SimpleStringProperty(data.toString());
	}
	
	public String getStringXModifiedDate() {
		return XModifiedDate.get();
	}
	
	public String getStringGoodModifiedDate() {
		Date date = new Date(Long.parseLong(XModifiedDate.get()));
		Format foramt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return foramt.format(date);
		
		//Date data = new Date(Long.parseLong(XModifiedDate.get()));
		//return data.toString();
	}
	
	public void setXModifiedDate(String XModifiedDate) {
		this.XModifiedDate.set(XModifiedDate);
		
		Date date = new Date(Long.parseLong(XModifiedDate));
		Format foramt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		this.xmd.set(foramt.format(date));
	}
	
	public StringProperty getxmd() {
		return xmd;
	}
	
	public Long getLongXModifiedDate() {
		return Long.parseLong(XModifiedDate.get());
	}

/* CONTENT-TYPE */
	public StringProperty getContentType() {
		return contentType;
	}

	public String getStringContentType() {
		return contentType.get();
	}
	
	public void setContentType(String content) {
		this.contentType.set(content);
	}
	
/* X-Uniform-Type-Identifier */
	public StringProperty getXUniformTypeIdentifier() {
		return XUniformTypeIdentifier;
	}
	
	public void setXUniformTypeIdentifier(String XUniformTypeIdentifier) {
		this.XUniformTypeIdentifier.set(XUniformTypeIdentifier);
	}
	
/* X-UNIFORM-TYPE-IDENTIFIER i MESSAGE-ID*/
	public StringProperty getXUniversallyUniqueIdentifier() {
		return XUniversallyUniqueIdentifier;
	}
	
	public String getStringXUniversallyUniqueIdentifier() {
		return XUniversallyUniqueIdentifier.get();
	}
	
	public void setXUniversallyUniqueIdentifier(String XUniversallyUniqueIdentifier) {
		this.XUniversallyUniqueIdentifier.set(XUniversallyUniqueIdentifier);
	}
	
/* MESSAGE GROUP */
	public StringProperty getMessageGroup() {
		return messageGroup;
	}
	
	public void setMessageGroup(String MessageGroup) {
		this.messageGroup.set(MessageGroup);
	}
	
	public String getStringMessageGroup() {
		return messageGroup.get();
	}
	
/* MessageID */
	
	public StringProperty getMessageID() {
		return MessageID;
	}

	public void setMessageID(StringProperty messageID) {
		MessageID = messageID;
	}
	
/* attachsList */
	
	public void setAttachsList(ObservableList<Zalacznik> attachs) {
		this.attachsList = attachs;
	}

	public ObservableList<Zalacznik> getAttachsList() {
		return attachsList;
	}
}

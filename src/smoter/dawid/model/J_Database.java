package smoter.dawid.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class J_Database {
	
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String DB_URL = "jdbc:sqlite:notes.db";
	
	private Connection c = null;
	private Statement stat;
	
	public J_Database() {
		
			try {
				Class.forName(J_Database.DRIVER);
			} catch (ClassNotFoundException e) {
				System.err.println("Brak sterownika JDBC");
				e.printStackTrace();
			}
			
			try {
				c = DriverManager.getConnection(DB_URL);
				stat = c.createStatement();
			} catch (SQLException e) {
				System.err.println("Problem z otwarciem połączenia do bazy danych");
				e.printStackTrace();
			}
			
		createTables();
	}
	
	public boolean createTables() {
		String createFolders = 	"CREATE TABLE IF NOT EXISTS folders " + 
				"(folderID	INTEGER	PRIMARY KEY AUTOINCREMENT , " +
				"folderName	TEXT NOT NULL, " +
				"UNIQUE(folderID, folderName));";
		String createNotes = "CREATE TABLE IF NOT EXISTS notes " +
				"(noteID CHAR(56) PRIMARY KEY NOT NULL, " +	
				"noteName	TEXT NOT NULL, " +
				"status	INTEGER NOT NULL, " +
				"createDate	INTEGER NOT NULL, " +
				"modifyDate INTEGER NOT NULL, " +
				"folderID INTEGER, " +
				"lastDate	INTEGER NOT NULL, " +
				"FOREIGN KEY(folderID) REFERENCES folders(folderID));";
		String createAttachments = 	"CREATE TABLE IF NOT EXISTS attachments" +
				"(attachID	INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"attachName	TEXT NOT NULL, " + 
				"extension	TEXT NOT NULL, " +
				"noteID	CHAR(56), " +
				"FOREIGN KEY(noteID) REFERENCES notes(noteID));";
		String createFirstFolder = "INSERT OR IGNORE into folders (folderID, folderName) values (1, 'Main');"; 
		
		try {
			stat.execute(createFolders);
			stat.execute(createNotes);
			stat.execute(createAttachments);
			stat.execute(createFirstFolder);
		} catch (SQLException e) {
			System.err.println("Błąd podczas tworzenia baz");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean insertMessages(ObservableList<GNote> lista) {
		for(GNote obiekt : lista) {
			try {
				ResultSet rs = stat.executeQuery("SELECT count(*) as ilosc FROM notes WHERE noteID='" + obiekt.getStringXUniversallyUniqueIdentifier() +"';");
				
				if(rs.getInt("ilosc")==0){
				PreparedStatement prepStmt = c.prepareStatement("INSERT into notes values (?,?,date(?),date(?),1);");
				prepStmt.setString(1, obiekt.getStringXUniversallyUniqueIdentifier());
				prepStmt.setString(2, obiekt.getStringSubject());
				prepStmt.setString(3, obiekt.getStringGoodCreatedDate());
				prepStmt.setString(4, obiekt.getStringGoodCreatedDate());
				prepStmt.execute();
				} else {
					System.err.println("Istnieje wpis o takim ID");
					return false;
				}
			} catch (SQLException e) {
				System.err.println("Błąd podczas insertu do bazy: createNote");
				e.printStackTrace();
				return false;
			}
			
		}
		return true;
	}
	
	public boolean insertMessage(GNote mes, int kind) {
		try {
			ResultSet rs = stat.executeQuery("SELECT count(*) as ilosc FROM notes WHERE noteID='" + mes.getStringXUniversallyUniqueIdentifier() +"';");
			if(rs.getInt("ilosc")==0){
				PreparedStatement prepStmt = c.prepareStatement("INSERT into notes values (?,?,?,?,?,?,?);");
				prepStmt.setString(1, mes.getStringXUniversallyUniqueIdentifier());
				prepStmt.setString(2, mes.getStringSubject());
				prepStmt.setInt(3, kind);
				prepStmt.setString(4, mes.getStringXCreatedDate());
				prepStmt.setString(5, mes.getStringXModifiedDate());
				prepStmt.setString(6, mes.getStringMessageGroup());
				prepStmt.setString(7, mes.getStringLastModifiedDate());
				prepStmt.execute();
			} else {
				System.err.println("Istnieje już wiadomość o takim ID w bazie danych.");
				return false;
			}
		} catch (SQLException e) {
			System.err.println("Błąd podczas insertu do bazy danych: insertMessage");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public HashMap<String, GNote> getMessages() {
		try {
			ResultSet rs = stat.executeQuery("SELECT noteID, modifyDate, lastDate FROM notes;");
			HashMap<String, GNote> dane = new HashMap<>();
			while(rs.next()) {
				dane.put(rs.getString("noteID"), new GNote(rs.getString("noteID"),rs.getString("modifyDate"),rs.getString("lastDate")));
			}
			return dane;
		} catch (SQLException e) {
			System.err.println("J_Database: getMessages error.");
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean deleteMessage(String nazwa) {
		
		try {
			String sql = "DELETE from notes WHERE noteID = '" + nazwa + "';";
			stat.executeUpdate(sql);
			return true;
		} catch(SQLException e) {
			System.err.println("J_Database: deleteMessage error.");
			e.printStackTrace();
			return false;
		}
	}
	
	public List<String> getNoteData(String id) {
		List<String> dane = new ArrayList<String>();
		try {
			ResultSet gnd = stat.executeQuery("SELECT noteName, createDate, modifyDate FROM notes WHERE noteID = '" + id + "';");
			//String[] dane = new String[3];
			dane.add(gnd.getString("noteName"));
			dane.add(gnd.getString("createDate"));
			dane.add(gnd.getString("modifyDate"));
			//dane[0] = gnd.getString("noteName");
			//dane[1] = gnd.getString("createDate");
			//dane[2] = gnd.getString("modifyDate");
			return dane;
		} catch (SQLException e) {
			System.err.println("J_Database: getNoteData error.");
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean updateLastSyncDate(String head) {
		PreparedStatement pr2;
		try {
			pr2 = c.prepareStatement("UPDATE notes set lastDate = modifyDate WHERE noteID = ?");
			pr2.setString(1, head);
			pr2.execute();
			return true;
		} catch (SQLException e) {
			System.err.println("J_Database: updateLastSyncDate error;");
			e.printStackTrace();
			return false;
		}
		
	}
	
	public ObservableList<String> getNoteAttachs(String id) {
		ObservableList<String> pliki = FXCollections.observableArrayList();
		try {
			ResultSet atc = stat.executeQuery("SELECT extension FROM attachments WHERE noteID = '" + id + "';");
			while(atc.next()) {
				pliki.add(atc.getString("extension"));
			}
		return pliki;
		} catch (SQLException e) {
			System.err.println("J_Database: getNoteAttachs error.");
			e.printStackTrace();
			return null;
		}
	}
	
	public void checkAttachments(ObservableList<Zalacznik> lista, String id) throws IOException {
		try {
			ResultSet rs = stat.executeQuery("SELECT attachName, extension FROM attachments WHERE noteID = '" + id + "';");
			List<Zalacznik> localAttachs = new ArrayList<Zalacznik>();
			
			while(rs.next()) {
				localAttachs.add(new Zalacznik(rs.getString("attachName"), rs.getString("extension")));
			}
			
			List<Zalacznik> dousuniecia = new ArrayList<Zalacznik>(localAttachs);
			
			
			// loop po listach
			for(Zalacznik zal : localAttachs) {
				if(lista.contains(zal)) {
					dousuniecia.remove(zal);	// usun wiadomosc z listy do usuniecia
					lista.remove(zal);		// usun z listy do dodania
				}
			}
			
			for(Zalacznik zal : dousuniecia) {
				String sql = "DELETE from attachments WHERE noteID = '" + id + "' AND attachName ='" + zal.getFileName() + "';";
				stat.executeUpdate(sql);
				File file = new File(System.getProperty("user.dir") + zal.getPath());
				file.delete();
			}
			
			for(Zalacznik zal : lista) {
				PreparedStatement prepStmt = c.prepareStatement("INSERT into attachments (attachName, extension, noteID) values (?,?,?);");
				prepStmt.setString(1, zal.getFileName());
				prepStmt.setString(2, "/attachments/" + id.substring(1, 37) + "/" + zal.getFileName());
				prepStmt.setString(3, id);
				prepStmt.execute();
				
				File a = new File(zal.getPath());
				File b = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37));
				File c = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37) + "/" + zal.getFileName());
				b.mkdirs();
				Files.copy(a.toPath(), c.toPath());
				
			}
			
		} catch (SQLException e) {
			System.err.println("J_Database: checkAttachments error.");
			e.printStackTrace();
		}
	}
	
	public void updateAttachments(BodyPart m, String id) {
		try {
			PreparedStatement prepStmt = c.prepareStatement("INSERT into attachments (attachName, extension, noteID) values (?,?,?);");
			prepStmt.setString(1, m.getFileName());
			prepStmt.setString(2, "/attachments/" + id.substring(1, 37) + "/" + m.getFileName());
			prepStmt.setString(3, id);
			prepStmt.execute();
		} catch(SQLException e) {
			System.err.println("J_Database: updateAttachments SQLException");
		} catch (MessagingException e) {
			System.err.println("J_Database: updateAttachments MessagingException");
			e.printStackTrace();
		}
	}
	
	public void checkRemoteAttachments(ObservableList<Zalacznik> lista, String id) throws IOException {
		if(lista.size() != 0){
		try {
			ResultSet rs = stat.executeQuery("SELECT attachName, extension FROM attachments WHERE noteID = '" + id + "';");
			List<Zalacznik> localAttachs = new ArrayList<Zalacznik>();
			
			// przenieś wiadomości z bazy do listy
			
			while(rs.next()) {
				localAttachs.add(new Zalacznik(rs.getString("attachName"), rs.getString("extension")));
			}
			
			List<Zalacznik> dousuniecia = new ArrayList<Zalacznik>(localAttachs);		
			
			// loop po listach
			for(Zalacznik zal : localAttachs) {
				if(lista.contains(zal)) {
					dousuniecia.remove(zal);	// usun wiadomosc z listy do usuniecia
					lista.remove(zal);		// usun z listy do dodania
				}
			}
			
			for(Zalacznik zal : dousuniecia) {
				String sql = "DELETE from attachments WHERE noteID = '" + id + "' AND attachName ='" + zal.getFileName() + "';";
				stat.executeUpdate(sql);
				File file = new File(System.getProperty("user.dir") + zal.getPath());
				file.delete();
			}

			for(Zalacznik zal : lista) {
				PreparedStatement prepStmt = c.prepareStatement("INSERT into attachments (attachName, extension, noteID) values (?,?,?);");
				prepStmt.setString(1, zal.getFileName());
				prepStmt.setString(2, "/attachments/" + id.substring(1, 37) + "/" + zal.getFileName());
				prepStmt.setString(3, id);
				prepStmt.execute();
				File b = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37));
				b.mkdirs();
		        File f = new File(System.getProperty("user.dir") + "/attachments/" + id.substring(1, 37) + "/" + zal.getFileName());
		        FileOutputStream fos = new FileOutputStream(f);
		        byte[] buf = new byte[4096];
		        int bytesRead;
		        while((bytesRead = zal.getIn().read(buf))!=-1) {
		            fos.write(buf, 0, bytesRead);
		        }
		        fos.close();
				
			}
			
		} catch (SQLException e) {
			System.err.println("J_Database: checkAttachments error.");
			e.printStackTrace();
		
		}
		}
	}
	
	public void updateMessage(MimeMessage m) {
		Object content;
		ObservableList<Zalacznik> remoteList = FXCollections.observableArrayList();
		try {
			content = m.getContent();
			 if (content.getClass().isAssignableFrom(MimeMultipart.class)) {
				 MimeMultipart multipart = (MimeMultipart) content;
				 for(int i=0;i<multipart.getCount();i++) {
					 BodyPart bodyPart = multipart.getBodyPart(i);
					 if(bodyPart.getFileName() != null)
					 remoteList.add(new Zalacznik(bodyPart.getFileName(), bodyPart.getInputStream()));
			    }
			 }
			checkRemoteAttachments(remoteList, m.getHeader("X-Universally-Unique-Identifier", ":"));
			updateAfterEdit(new GNote(m), true);
		} catch (IOException e) {
			System.err.println("MainOverviewController: getAttachs IOException error.");
			e.printStackTrace();
		} catch (MessagingException e) {
			System.err.println("MainOverviewController: getAttachs MessagingException error.");
			e.printStackTrace();
		}
	}
	
	public String changeID(String ID) {
		String newID = "<" + String.valueOf(UUID.randomUUID()) + "@email.android.com>";
			
		try {
				PreparedStatement prepStmt = c.prepareStatement("INSERT INTO notes SELECT '" + newID + "', noteName, status, createDate, modifyDate, folderID, modifyDate FROM notes WHERE noteID = ?;");
				prepStmt.setString(1, ID);
				prepStmt.execute();
				PreparedStatement prepStmt2 = c.prepareStatement("UPDATE attachments set noteID = ?, extension = ? || (SELECT attachName FROM attachments WHERE noteID = ? ) WHERE noteID = ?;");
				prepStmt2.setString(1, newID);
				prepStmt2.setString(2, "/attachments/" + newID.substring(1, 37) + "/");
				prepStmt2.setString(3, ID);
				prepStmt2.setString(4, ID);
				prepStmt2.execute();
				File file = new File(System.getProperty("user.dir") + "/attachments/" + ID.substring(1, 37));
				if(file.exists()) {
					File file2 = new File(System.getProperty("user.dir") + "/attachments/" + newID.substring(1, 37));
					file.renameTo(file2);
				}
				return newID;
			} catch (SQLException e) {
				System.err.println("J_Database: changeID error.");
				e.printStackTrace();
				return "";
			}
		
	}
	
	public int createTab(String nazwa) {
		try {
			ResultSet rs = stat.executeQuery("SELECT count(*) as ilosc FROM folders WHERE folderName='" + nazwa + "';");
			if(rs.getInt("ilosc")==0){
				PreparedStatement prepStmt = c.prepareStatement("INSERT into folders (folderName) values (?);");
				prepStmt.setString(1, nazwa);
				prepStmt.execute();
				ResultSet prepStmt2 = stat.executeQuery("SELECT folderID as ID FROM folders WHERE folderName='" + nazwa + "';");
				return(prepStmt2.getInt("ID"));
			} else {
				System.err.println("Tab o takiej nazwie już istnieje w bazie danych");
				return 0;
			}
		} catch (SQLException e) {
			System.err.println("Błąd podczas insertu do bazy: createTab");
			e.printStackTrace();
			return 0;
		}
	}
	
	public int openTab(String nazwa) {
		try {
			ResultSet rs = stat.executeQuery("SELECT count(*) as ilosc, folderID as ID FROM folders WHERE folderName='" + nazwa + "';");
			if(rs.getInt("ilosc")==1) {
				return rs.getInt("ID");
			} else {
				System.err.println("DB_openTab: Nie ma taba o takiej nazwie w bazie danych.");
				return 0;
			}
		} catch (SQLException e) {
			System.err.println("Błąd podczas selectu do bazy: openTab");
			e.printStackTrace();
			return 0;
		}
	}
	
	public List<String> getTabs() {
		List<String> lista = new ArrayList<String>();
		try {
			ResultSet rs = stat.executeQuery("SELECT folderName as nazwa FROM folders LIMIT -1 OFFSET 1;");
			while(rs.next()) {
				lista.add(rs.getString("nazwa"));
			}
			return lista;
		} catch (SQLException e) {
			System.err.println("J_Database: getTabs error.");
			e.printStackTrace();
			return null;
		}
	}
	
	public ObservableList<GNote> getMessagesToTab(String id, int folderID) {
		ObservableList<GNote> lista = FXCollections.observableArrayList();
			try {
				ResultSet rs = stat.executeQuery("SELECT noteID, noteName, createDate, modifyDate FROM notes WHERE folderID = '" + folderID + "';");
				while(rs.next()) {
					lista.add(new GNote(rs.getString("noteID"), rs.getString("noteName"), rs.getString("createDate"), rs.getString("modifyDate"), folderID));
				}
			return lista;
			} catch (SQLException e) {
				System.err.println("J_Database: getMessagesToTab error.");
				e.printStackTrace();
				return null;
			}
	}
	
	public void updateAfterEdit(GNote wiadomosc, boolean changeLastSyncDate) {
		String sql = "UPDATE notes set noteName = '" + wiadomosc.getStringSubject() + "' WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() +  "';";
		String sql2 = "UPDATE notes set createDate = '" + wiadomosc.getStringXCreatedDate() + "' WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() +  "';";
		String sql3 = "UPDATE notes set modifyDate = '" + wiadomosc.getStringXModifiedDate() + "' WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() +  "';";
		String sql4 = "UPDATE notes set folderID = '" + wiadomosc.getMessageGroup().get() +  "' WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() +  "';";
		String sql5 = "UPDATE notes set status = 2 WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() + "';";
		try {
			stat.executeUpdate(sql);
			stat.executeUpdate(sql2);
			stat.executeUpdate(sql3);
			stat.executeUpdate(sql4);
			stat.executeUpdate(sql5);
			if(changeLastSyncDate)
				stat.executeUpdate("UPDATE notes set lastDate = '" + wiadomosc.getStringLastModifiedDate() + "' WHERE noteID = '" + wiadomosc.getStringXUniversallyUniqueIdentifier() + "';");
			
		} catch (SQLException e) {
			System.err.println("J_Database -> updateAfterEdit : ERROR");
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ObservableList<Zalacznik> selectAttachs(String id) {
		ObservableList<Zalacznik> lista = FXCollections.observableArrayList();
		try {
			ResultSet rs = stat.executeQuery("SELECT attachName, extension FROM attachments WHERE noteID = '" + id + "';");
			while(rs.next()) {
				lista.add(new Zalacznik(rs.getString("attachName"),rs.getString("extension")));
			}

			return lista;
		} catch (SQLException e) {
			System.err.println("J_Database -> selectAttachs: ERROR: ");
			e.printStackTrace();
			return null;
		}
	}
	
	public String getFolderID(String nazwa) {
		try {
			ResultSet rs = stat.executeQuery("SELECT folderID as ID FROM notes WHERE noteID = '" + nazwa + "';");
			return rs.getString("ID");
		} catch (SQLException e) {
			System.err.println("getFolderID: coś poszło nie tak.");
			e.printStackTrace();
			return "";
		}
	}
	
	public String getFolderName(String id) {
		try {
			ResultSet rs = stat.executeQuery("SELECT folderName as Name FROM folders WHERE folderID = '" + id + "';");
			return rs.getString("Name");
		} catch (SQLException e) {
			System.err.println("getFolderName: coś poszło nie tak.");
			e.printStackTrace();
			return "";
		}
	}
	
}
